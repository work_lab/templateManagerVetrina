const express = require('express');
const server = express();
const request = require('request');
const proxy = require('http-proxy-middleware');
//var jsonfile = require('jsonfile')
const fs = require('fs');
server.set('view engine', 'ejs');

const createProxy = (path, target) =>
  server.use(path, proxy({ target, changeOrigin: true, pathRewrite: {[`^${path}`]: '',}, onProxyReq: function (proxyReq, req, res) {
    //console.log(req.query);
    console.log("Session: %j",req.headers['param']);
    if (req.headers['param']!="" && req.headers['param']!=undefined) {
      
      proxyReq.setHeader('param', JSON.stringify(req.headers['param']));
    }
   

} }));

var fragRoutes = fs.readFileSync('fragmentRoutes.json', 'utf8');
var fragRoutesJson =JSON.parse(fragRoutes);
for (var index = 0; index < fragRoutesJson.length; index++) {
  console.log(fragRoutesJson[index])
  createProxy(fragRoutesJson[index].name, fragRoutesJson[index].url);
}

/*createProxy('/header', 'https://microfrontends-header.herokuapp.com/');
createProxy('/products-list', 'http://localhost:3000');
createProxy('/cart', 'https://microfrontends-cart.herokuapp.com/');*/
var carrello="";
server.get('/', (req, res) => res.render('index'));
server.get('/dettaglio', (req, res) =>{ 

  res.render('dettaglio',{parameters:req.query});
});
const port = process.env.PORT || 8081;
server.listen(port, () => {
  console.log(`Homepage listening on port ${port}`);
});
function getJson(file){
  var fileret = fs.readFileSync(file, 'utf8');
  return JSON.parse(fileret);
}